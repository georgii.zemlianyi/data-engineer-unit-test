
import org.apache.kafka.clients.consumer.ConsumerRecord
import org.apache.spark.{SparkConf, SparkContext}
import org.apache.spark.sql.{DataFrame, Row, SparkSession}
import org.apache.spark.sql.types.{LongType, StringType, StructField, StructType}
import org.apache.spark.streaming.dstream.{DStream, InputDStream}
import org.apache.spark.streaming.kafka010.{ConsumerStrategies, KafkaUtils, LocationStrategies}
import org.apache.spark.streaming.{Duration, Seconds, StreamingContext}
object JobDStream {
  type DataWriter = DataFrame => Unit
  object ParquetWriter extends DataWriter {
    def apply(df: DataFrame): Unit = {
      df.coalesce(1).write.mode("append").parquet("parquet_writed")
    }
  }
  def createContext(): StreamingContext = {

    // Stream setup
    val sparkConf = new SparkConf().setAppName("DStream").setMaster("local[*]")
    val spark = SparkSession.builder().config(sparkConf).getOrCreate()
    val ssc = new StreamingContext(spark.sparkContext, Seconds(1))
    val kafkaParams = Map[String, String](
      "bootstrap.servers" -> "localhost:9092",
      "key.deserializer" -> "org.apache.kafka.common.serialization.StringDeserializer",
      "value.deserializer" -> "org.apache.kafka.common.serialization.StringDeserializer",
      "group.id" -> "groupId1",
      "auto.offset.reset" -> "earliest"
    )
    val topicsSet = Set("topic1")
    val schema = new StructType()
      .add(StructField("Name", StringType, false))

    // Create input stream
    val inputStream: InputDStream[ConsumerRecord[String, String]] = KafkaUtils.createDirectStream[String, String](
      ssc,
      LocationStrategies.PreferConsistent,
      ConsumerStrategies.Subscribe[String, String](topicsSet, kafkaParams)
    )

    // Process and write stream
    processStream(inputStream, spark, schema)(ParquetWriter)

    ssc
  }

  def processStream(inputStream: InputDStream[ConsumerRecord[String, String]], spark: SparkSession, schema: StructType)
                   (dataWriter: DataWriter): Unit = {
    inputStream.foreachRDD((rdd, _) => {
      val rowRDD = rdd.map(row => Row(row.value()))
      val df = spark.createDataFrame(rowRDD, schema)
      dataWriter(df)
    })
  }

  def processForTest(inputStream: DStream[ String]): DStream[String] = {
    inputStream.map(record => record+"End")
  }

  def main(args: Array[String]): Unit = {
    val ssc = createContext()

    ssc.start()
    ssc.awaitTermination()
  }
}


