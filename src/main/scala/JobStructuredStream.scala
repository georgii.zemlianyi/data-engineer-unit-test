import org.apache.spark.SparkConf
import org.apache.spark.sql.streaming.{StreamingQuery, Trigger}
import org.apache.spark.sql.{DataFrame, SparkSession}

object JobStructuredStream {

  type DataWriter = DataFrame => StreamingQuery

  object ParquetWriter extends DataWriter {
    override def apply(df: DataFrame): StreamingQuery = {
      df.writeStream.format("parquet")
        .option("path", "home")
        .trigger(Trigger.ProcessingTime("10 minutes"))
        .start()
    }
  }

  def createQuery(): StreamingQuery = {
    // Stream setup
    val sparkConf = new SparkConf().setAppName("StructuredStream")
    val spark = SparkSession.builder().config(sparkConf).getOrCreate()

    // Create input stream
    val df = spark.readStream
      .format("kafka")
      .option("kafka.bootstrap.servers", "host1:port1,host2:port2")
      .option("subscribe", "topic1")
      .load()

    processStream(df)(ParquetWriter)
  }

  def processStream(df: DataFrame)(dataWriter: DataWriter): StreamingQuery = {
    val dfToSave = df.selectExpr("CAST(value AS STRING)")
    dataWriter(dfToSave)
  }

  def main(args: Array[String]): Unit = {
    val query = createQuery()

    query.awaitTermination()
    query.stop()
  }
}
