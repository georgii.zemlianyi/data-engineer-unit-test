import org.apache.kafka.clients.consumer.ConsumerRecord
import org.apache.spark.SparkConf
import org.apache.spark.rdd.RDD
import org.apache.spark.sql.types.{LongType, StructField, StructType}
import org.apache.spark.sql.{DataFrame, SparkSession}
import org.apache.spark.streaming.dstream.InputDStream
import org.apache.spark.streaming.kafka010.{ConsumerStrategies, KafkaUtils, LocationStrategies}
import org.apache.spark.streaming.{Seconds, StreamingContext, Time}

object WindowDStream {
  type DataWriter = (RDD[Long], Time) => Unit

  object TextFileWriter extends DataWriter with Serializable {
    def apply(rdd: RDD[Long], time: Time): Unit = {
      rdd.saveAsTextFile(time.milliseconds + "-some.txt")
    }
  }

  def createContext(): StreamingContext = {

    // Stream setup
    val sparkConf = new SparkConf()
      .setAppName("KafkaDStream")

    val spark = SparkSession.builder().config(sparkConf).getOrCreate()
    val ssc = new StreamingContext(spark.sparkContext, Seconds(1))
    val kafkaParams = Map[String, String](
      "bootstrap.servers" -> "localhost:9092",
      "key.deserializer" -> "org.apache.kafka.common.serialization.StringDeserializer",
      "value.deserializer" -> "org.apache.kafka.common.serialization.StringDeserializer",
      "group.id" -> "groupId1",
      "auto.offset.reset" -> "earliest"
    )
    val topicsSet = Set("topic1")
    val schema = new StructType()
      .add(StructField("Count", LongType, false))

    // Create input stream
    val inputStream: InputDStream[ConsumerRecord[String, String]] = KafkaUtils.createDirectStream[String, String](
      ssc,
      LocationStrategies.PreferConsistent,
      ConsumerStrategies.Subscribe[String, String](topicsSet, kafkaParams)
    )

    // Process and write stream
    processWindow(inputStream, spark, schema)(TextFileWriter)
    ssc.checkpoint("checkpoint_directory")

    ssc
  }

  def processWindow(inputStream: InputDStream[ConsumerRecord[String, String]], spark: SparkSession, schema: StructType)
                   (dataWriter: DataWriter): Unit = {
    inputStream.countByWindow(Seconds(3), Seconds(3))
      .foreachRDD(
        (rdd, time) => {
          dataWriter(rdd, time)
        }
      )
  }

  def main(args: Array[String]): Unit = {
    val ssc = createContext()

    ssc.start()
    ssc.awaitTermination()
  }
}
