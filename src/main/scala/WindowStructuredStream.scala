import org.apache.spark.SparkConf
import org.apache.spark.sql.functions.{col, count, explode, expr, from_json, to_timestamp, window}
import org.apache.spark.sql.{DataFrame, SparkSession, functions}
import org.apache.spark.sql.streaming.StreamingQuery
import org.apache.spark.sql.types.{LongType, StringType, StructField, StructType}

object WindowStructuredStream {
  type DataWriter = DataFrame => StreamingQuery
  val schema = new StructType()
    .add(StructField("sensor_id", StringType, true))
    .add(StructField("EventTime", StringType, true))
    .add(StructField("Value", LongType, true))
  object ParquetWriter extends DataWriter {
    override def apply(df: DataFrame): StreamingQuery = {
      df.writeStream.format("parquet")
        .queryName("hello")
        .option("path", "home")
        .option("checkpointLocation", "checkpoint_dir")
        .outputMode("append")
        .start()
    }
  }

  def createQuery(): StreamingQuery = {
    // Stream setup
    val sparkConf = new SparkConf().setAppName("StructuredStream").setMaster("local[*]")
    val spark = SparkSession.builder().config(sparkConf).getOrCreate()

    // Create input stream
    val df = spark.readStream
      .format("kafka")
      .option("kafka.bootstrap.servers", "localhost:9092")
      .option("subscribe", "topic1")
      .option("startingOffsets", "earliest")
      .load()

    processEventTimeWindow(df)(ParquetWriter)
  }

  def processEventTimeWindow(df: DataFrame)(dataWriter: DataWriter): StreamingQuery = {
    val baseDF = df.selectExpr("CAST(value as STRING)", "timestamp")

    val infoDF = baseDF.select(from_json(col("value"), schema).alias("sample"), col("timestamp"))

    val tableDF = infoDF.select("sample.*", "timestamp").withColumn("EventTime", to_timestamp(col("EventTime")))
    val finalDF = tableDF.withWatermark("EventTime", "1 second")
      .groupBy(col("sensor_id"), window(col("EventTime"), "1 minute"))
      .agg(
        expr("max_by(Value, EventTime)").alias("lastValue"),
        expr("min_by(Value, EventTime)").alias("firstValue"),
        functions.max("EventTime").alias("lastEventTime"),
        functions.min("EventTime").alias("firstEventTime"),
        functions.count("Value").alias("count")
      )
    dataWriter(finalDF)
  }

  def processIngestionTimeWindow(df: DataFrame)(dataWriter: DataWriter): StreamingQuery = {
    val baseDF = df.selectExpr("CAST(value as STRING)", "timestamp")

    val infoDF = baseDF.select(from_json(col("value"), schema).alias("sample"), col("timestamp"))

    val tableDF = infoDF.select("sample.*", "timestamp").withColumn("EventTime", to_timestamp(col("EventTime")))
    val finalDF = tableDF.withWatermark("timestamp", "1 second")
      .groupBy(col("sensor_id"), window(col("timestamp"), "1 second"))
      .agg(
        expr("max_by(Value, timestamp)").alias("lastValue"),
        expr("min_by(Value, timestamp)").alias("firstValue"),
        functions.max("timestamp").alias("lastEventTime"),
        functions.min("timestamp").alias("firstEventTime"),
        functions.count("Value").alias("count")
      )
    dataWriter(finalDF)
  }

  def doNothing(df: DataFrame)(dataWriter: DataWriter): StreamingQuery = {
    val baseDF = df.selectExpr("CAST(value as STRING)", "timestamp")

    val infoDF = baseDF.select(from_json(col("value"), schema).alias("sample"), col("timestamp"))

    val tableDF = infoDF.select("sample.*", "timestamp")
    dataWriter(tableDF)
  }

  def main(args: Array[String]): Unit = {
    val query = createQuery()

    query.awaitTermination()
    query.stop()
  }
}
