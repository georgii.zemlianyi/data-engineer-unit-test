import JobDStream.{ParquetWriter, processStream}
import org.apache.kafka.clients.consumer.ConsumerRecord
import org.apache.spark.SparkConf
import org.apache.spark.rdd.RDD
import org.apache.spark.sql.{Row, SparkSession}
import org.apache.spark.sql.types.{StringType, StructField, StructType}
import org.apache.spark.streaming.{Seconds, StreamingContext}
import org.apache.spark.streaming.dstream.InputDStream
import org.scalatest.concurrent.Eventually
import org.scalatest.flatspec.AnyFlatSpec
import org.scalatest.time.{Millis, Span}
import testutil.SparkStreamingSpec

import scala.collection.mutable
import scala.collection.mutable.ListBuffer

class JobDStreamSpecManual extends  AnyFlatSpec with SparkStreamingSpec with Eventually {
  implicit override val patienceConfig =
    PatienceConfig(timeout = scaled(Span(5000, Millis)))


  "it" should  "work with manual clock" in {
    val lines = mutable.Queue[RDD[ConsumerRecord[String, String]]]()
    val results = ListBuffer.empty[Row]
    val inputStream: InputDStream[ConsumerRecord[String, String]] = ssc.queueStream(lines)
    val expected = Array(Row("body1"), Row("body2"), Row("body3"), Row("body4"))

    // Act
    processStream(inputStream, spark, schema)(df => results ++= df.collect())

    ssc.start()
    lines += spark.sparkContext.parallelize(Seq(
      new ConsumerRecord("topic", 0, 0, "key1", "body1"),
      new ConsumerRecord("topic", 0, 0, "key2", "body2"))
    )
    lines += spark.sparkContext.parallelize(Seq(
      new ConsumerRecord("topic", 0, 0, "key3", "body3"),
      new ConsumerRecord("topic", 0, 0, "key4", "body4"))
    )
    advanceClockOneBatch()
    advanceClockOneBatch()

    // Assert
    eventually(
      assert(results.sameElements(expected))
    )
  }


  val schema = new StructType()
    .add(StructField("Name", StringType, false))
}
