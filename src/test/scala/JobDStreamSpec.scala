import JobDStream.processStream
import org.apache.kafka.clients.consumer.ConsumerRecord
import org.apache.spark.SparkConf
import org.apache.spark.rdd.RDD
import org.apache.spark.sql.types.{StringType, StructField, StructType}
import org.apache.spark.sql.{Row, SparkSession}
import org.apache.spark.streaming.dstream.InputDStream
import org.apache.spark.streaming.{Seconds, StreamingContext}
import org.scalatest.flatspec.AnyFlatSpec

import scala.collection.mutable
import scala.collection.mutable.ListBuffer

class JobDStreamSpec extends AnyFlatSpec{

  "it" should "not work" in {
    // Arrange
    val sparkConf = new SparkConf().setAppName("TrendingHashtags")
      .setMaster("local[*]")
      .set("spark.serializer", "org.apache.spark.serializer.KryoSerializer")

    val spark = SparkSession.builder().config(sparkConf).getOrCreate()
    val ssc = new StreamingContext(spark.sparkContext, Seconds(1))
    val kafkaMessages = mutable.Queue[RDD[ConsumerRecord[String, String]]]()
    val inputStream: InputDStream[ConsumerRecord[String, String]] = ssc.queueStream(kafkaMessages)

    kafkaMessages += spark.sparkContext.parallelize(Seq(
      new ConsumerRecord("topic", 0, 0, "key1", "body1"),
      new ConsumerRecord("topic", 0, 0, "key2", "body2"))
    )

    kafkaMessages += spark.sparkContext.parallelize(Seq(
      new ConsumerRecord("topic", 0, 0, "key3", "body3"),
      new ConsumerRecord("topic", 0, 0, "key4", "body4"))
    )

    val results = ListBuffer.empty[Row]

    // Act
    processStream(inputStream, spark, schema)(df => results ++= df.collect())

    ssc.start()
    ssc.awaitTerminationOrTimeout(5000L)

    // Assert
    val expected = Array(Row("body1"), Row("body2"), Row("body3"), Row("body4"))
    assert(results.sameElements(expected))
    ssc.stop()
  }

  val schema = new StructType()
    .add(StructField("Name", StringType, false))
}
