import WindowStructuredStream.{doNothing, processEventTimeWindow, processIngestionTimeWindow}
import io.github.embeddedkafka.{EmbeddedKafka, EmbeddedKafkaConfig}
import org.apache.kafka.common.serialization.{Deserializer, Serializer, StringDeserializer, StringSerializer}
import org.apache.spark.sql.{DataFrame, Row, SparkSession}
import org.scalatest.BeforeAndAfterEach
import org.scalatest.flatspec.AnyFlatSpec

/**
 * Spec for Window Function for Structured stream
 */
class WindowStructuredStreamSpec extends AnyFlatSpec with EmbeddedKafka with BeforeAndAfterEach {

  "event time based window" should "group data" in {
    // arrange
    publishToKafka(topic, str1)
    publishToKafka(topic, str2)
    publishToKafka(topic, str3)
    publishToKafka(topic, str4)
    publishToKafka(topic, str5)
    publishToKafka(topic, str6)
    publishToKafka(topic, str7)
    val (df, spark) = getKafkaDF()

    // action
    val query = processEventTimeWindow(df)(df => df.writeStream.queryName("resultsEventTime").format("memory").outputMode("complete").start())
    query.processAllAvailable()

    // assert
    assert(spark.sql("Select count from resultsEventTime").collect().sameElements(List(Row(2), Row(5))))
  }

  "ingestion time based window" should "group data differently" in {
    // arrange
    publishToKafka(topic, str1)
    publishToKafka(topic, str2)
    publishToKafka(topic, str3)
    publishToKafka(topic, str4)
    Thread.sleep(5000)
    publishToKafka(topic, str5)
    publishToKafka(topic, str6)
    publishToKafka(topic, str7)
    val (df, spark) = getKafkaDF()

    // action
    val query = processIngestionTimeWindow(df)(df => df.writeStream.queryName("resultsIngestionTime").format("memory").outputMode("complete").start())
    query.processAllAvailable()

    // assert
    assert(spark.sql("Select count from resultsIngestionTime").collect().sameElements(List(Row(4), Row(3))))
  }

  "event time based window" should "demonstrate without assert" in {
    // arrange
    publishToKafka(topic, str1)
    publishToKafka(topic, str2)
    publishToKafka(topic, str3)
    publishToKafka(topic, str4)
    publishToKafka(topic, str5)
    publishToKafka(topic, str6)
    publishToKafka(topic, str7)
    val (df, spark) = getKafkaDF()

    // action
    val query = processEventTimeWindow(df)(
      df => df.writeStream.format("console")
        .option("truncate", false)
        .outputMode("complete")
        .start()
    )
    query.processAllAvailable()
  }


  "ingestion time based window" should "demonstrate without assert" in {
    // arrange
    publishToKafka(topic, str1)
    publishToKafka(topic, str2)
    publishToKafka(topic, str3)
    publishToKafka(topic, str4)
    Thread.sleep(5000)
    publishToKafka(topic, str5)
    publishToKafka(topic, str6)
    publishToKafka(topic, str7)
    val (df, spark) = getKafkaDF()

    // action
    val query = processIngestionTimeWindow(df)(
      df => df.writeStream.format("console")
        .option("truncate", false)
        .outputMode("complete")
        .start()
    )
    query.processAllAvailable()
  }

  "just show function" should "show dataframe" in {
    // arrange
    publishToKafka(topic, str1)
    publishToKafka(topic, str2)
    publishToKafka(topic, str3)
    publishToKafka(topic, str4)
    Thread.sleep(5000)
    publishToKafka(topic, str5)
    publishToKafka(topic, str6)
    publishToKafka(topic, str7)
    val (df, spark) = getKafkaDF()

    // action
    val query = doNothing(df)(
      df => df.writeStream.format("console")
        .option("truncate", false)
        .start()
    )
    query.processAllAvailable()
  }
  override def afterEach(): Unit = {
    super.afterEach()
    EmbeddedKafka.stop()
  }

  override def beforeEach(): Unit = {
    super.beforeEach()
    EmbeddedKafka.start()
    createCustomTopic(topic = topic, partitions = 1)
  }
  def getKafkaDF(): (DataFrame, SparkSession) = {
    val spark = SparkSession.builder().appName("KafkaDeser").master("local[*]").getOrCreate()
    val df = spark.readStream.format("kafka")
      .option("kafka.bootstrap.servers", "localhost:9092")
      .option("subscribe", topic)
      .option("startingOffsets", "earliest")
      .load()
    (df, spark)
  }

  val topic = "topic2"
  implicit val embeddedKafkaConfig: EmbeddedKafkaConfig = EmbeddedKafkaConfig(kafkaPort = 9092)
  implicit val serializer: Serializer[String] = new StringSerializer()
  implicit val deserializer: Deserializer[String] = new StringDeserializer()

  val str1: String =
    """
      |{
      | "sensor_id": "sensor_1",
      | "EventTime": "2022-02-02 10:10:01",
      | "Value": 65534
      |}
      """.stripMargin

  val str2: String =
    """
      |{
      | "sensor_id": "sensor_1",
      | "EventTime": "2022-02-02 10:10:02",
      | "Value": 65535
      |}
      """.stripMargin

  val str3: String =
    """
      |{
      | "sensor_id": "sensor_1",
      | "EventTime": "2022-02-02 10:10:03",
      | "Value": 0
      |}
      """.stripMargin

  val str4: String =
    """
      |{
      | "sensor_id": "sensor_1",
      | "EventTime": "2022-02-02 10:10:04",
      | "Value": 1
      |}
      """.stripMargin

  val str5: String =
    """
      |{
      | "sensor_id": "sensor_1",
      | "EventTime": "2022-02-02 10:11:02",
      | "Value": 2
      |}
      """.stripMargin

  val str6: String =
    """
      |{
      | "sensor_id": "sensor_1",
      | "EventTime": "2022-02-02 10:11:04",
      | "Value": 4
      |}
      """.stripMargin

  val str7: String =
    """
      |{
      | "sensor_id": "sensor_1",
      | "EventTime": "2022-02-02 10:10:59",
      | "Value": 567
      |}
      """.stripMargin
}