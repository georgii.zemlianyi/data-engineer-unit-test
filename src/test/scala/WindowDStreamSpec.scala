
import WindowDStream.{TextFileWriter, createContext, processWindow}
import io.github.embeddedkafka.{EmbeddedKafka, EmbeddedKafkaConfig}
import org.apache.kafka.clients.consumer.ConsumerRecord
import org.scalatest.flatspec.AnyFlatSpec
import org.apache.kafka.common.serialization.{Deserializer, Serializer, StringDeserializer, StringSerializer}
import org.apache.spark.SparkConf
import org.apache.spark.sql.SparkSession
import org.apache.spark.sql.types.{LongType, StructField, StructType}
import org.apache.spark.streaming.dstream.InputDStream
import org.apache.spark.streaming.kafka010.{ConsumerStrategies, KafkaUtils, LocationStrategies}
import org.apache.spark.streaming.{Seconds, StreamingContext}

import scala.collection.mutable.ArrayBuffer

/**
 * Spec for Window Function for Structured stream
 */
class WindowDStreamSpec extends AnyFlatSpec with EmbeddedKafka {

  "kafka window with DStream" should "use ingestion time" in {
    // Stream setup
    val (spark, ssc, inputStream) = getSessionAndContext()
    EmbeddedKafka.start()
    createCustomTopic(topic = topic, partitions = 1)
    (1 to 5).foreach(i => publishToKafka(topic, "message_1_" + i))
    (1 to 5).foreach(i => publishToKafka(topic, "message_2_" + i))
    (1 to 5).foreach(i => publishToKafka(topic, "message_3_" + i))
    val result: ArrayBuffer[Long] = ArrayBuffer.empty[Long]

    // action
    processWindow(inputStream, spark, schema)((rdd, time) => result += rdd.collect()(0))

    // start and stop context
    ssc.start()
    ssc.awaitTerminationOrTimeout(1000)
    ssc.stop(true, true)
    deleteTopics(List(topic))
    EmbeddedKafka.stop()

    // assert
    assert(result.sum == 15)
  }

  def getSessionAndContext(): (SparkSession, StreamingContext, InputDStream[ConsumerRecord[String, String]]) = {
    val sparkConf = new SparkConf()
      .setAppName("DStream")
      .setMaster("local[*]")

    val spark = SparkSession.builder().config(sparkConf).getOrCreate()
    val ssc = new StreamingContext(spark.sparkContext, Seconds(1))


    // Create input stream
    val inputStream: InputDStream[ConsumerRecord[String, String]] = KafkaUtils.createDirectStream[String, String](
      ssc,
      LocationStrategies.PreferConsistent,
      ConsumerStrategies.Subscribe[String, String](topicsSet, kafkaParams)
    )
    ssc.checkpoint("checkpoint_directory")

    (spark, ssc, inputStream)
  }

  val topic = "topic1"
  val kafkaParams = Map[String, String](
    "bootstrap.servers" -> "localhost:9092",
    "key.deserializer" -> "org.apache.kafka.common.serialization.StringDeserializer",
    "value.deserializer" -> "org.apache.kafka.common.serialization.StringDeserializer",
    "group.id" -> "groupId1",
    "auto.offset.reset" -> "earliest"
  )
  val topicsSet = Set(topic)
  val schema = new StructType()
    .add(StructField("Count", LongType, false))
  implicit val embeddedKafkaConfig: EmbeddedKafkaConfig = EmbeddedKafkaConfig(kafkaPort = 9092)
  implicit val serializer: Serializer[String] = new StringSerializer()
  implicit val deserializer: Deserializer[String] = new StringDeserializer()
}
