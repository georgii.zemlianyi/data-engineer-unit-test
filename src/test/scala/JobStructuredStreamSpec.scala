import JobStructuredStream.{ParquetWriter, processStream}
import org.apache.spark.SparkConf
import org.apache.spark.sql.execution.streaming.MemoryStream
import org.apache.spark.sql.{Encoders, Row, SparkSession}
import org.scalatest.flatspec.AnyFlatSpec

import scala.collection.mutable.{ArrayBuffer, ListBuffer}

class JobStructuredStreamSpec extends AnyFlatSpec {

  "it" should "work with Structured Stream" in {
    val sparkConf = new SparkConf().setAppName("Test").setMaster("local[*]")
    val spark = SparkSession.builder().config(sparkConf).getOrCreate()
    val expected = Array(Row("A"), Row("B"))
    val inMemoryRecords = List("A", "B")

    val memStream = new MemoryStream[String](id = 1, spark.sqlContext)(Encoders.STRING)

    memStream.addData(inMemoryRecords)

    val df = memStream.toDF()

    // Act
    val streamingQuery = processStream(df)(df => df.writeStream.queryName("results").format("memory").start())
    streamingQuery.processAllAvailable()

    // Assert
    assert(spark.sql("Select * from results").collect().sameElements(expected))
  }
}
