package testutil

import org.apache.spark.sql.SparkSession
import org.apache.spark.SparkConf
import org.scalatest.{BeforeAndAfterAll, Suite}

trait SparkSpec extends BeforeAndAfterAll {
  this: Suite =>

  private var _spark: SparkSession = _

  override def beforeAll(): Unit = {
    super.beforeAll()

    val conf = new SparkConf()
      .setMaster("local[*]")
      .setAppName(this.getClass.getSimpleName)
      .set("spark.serializer", "org.apache.spark.serializer.KryoSerializer")

    sparkConfig.foreach { case (k, v) => conf.setIfMissing(k, v) }

    _spark = SparkSession.builder().config(conf).getOrCreate()
  }

  def sparkConfig: Map[String, String] = Map.empty

  override def afterAll(): Unit = {
    if (_spark != null) {
      _spark.stop()
      _spark = null
    }
    super.afterAll()
  }

  def spark: SparkSession = _spark

}

