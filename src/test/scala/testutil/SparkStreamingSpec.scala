package testutil

import org.apache.spark.streaming.{ClockWrapper, Duration, Seconds, StreamingContext}
import org.scalatest.Suite

import java.nio.file.Files

trait SparkStreamingSpec extends SparkSpec {
  this: Suite =>


  private var _ssc: StreamingContext = _
  private var _clock: ClockWrapper = _

  override def beforeAll(): Unit = {
    super.beforeAll()

    _ssc = new StreamingContext(spark.sparkContext, batchDuration)
    _clock = new ClockWrapper(_ssc)
  }

  def batchDuration: Duration = Seconds(1)

  override def afterAll(): Unit = {
    if (_ssc != null) {
      _ssc.stop(stopSparkContext = false, stopGracefully = false)
      _ssc = null
    }

    super.afterAll()
  }

  override def sparkConfig: Map[String, String] = {
    super.sparkConfig + ("spark.streaming.clock" -> "org.apache.spark.streaming.util.ManualClock")
  }

  def ssc: StreamingContext = _ssc

  def advanceClock(timeToAdd: Duration): Unit = {
    _clock.advance(timeToAdd.milliseconds)
  }

  def advanceClockOneBatch(): Unit = {
    advanceClock(Duration(batchDuration.milliseconds))
  }

}
