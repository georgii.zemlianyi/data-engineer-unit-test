import JobDStream.processForTest
import com.holdenkarau.spark.testing.StreamingSuiteBase
import org.scalatest.flatspec.AnyFlatSpec

class StreamingOperationsSpec extends AnyFlatSpec with StreamingSuiteBase {
  "it" should "work with StreamingSuitBase" in {
    // Arrange
    val input = List(
      List("body1"),
      List("body2")
    )
    val expected = List(List("body1End"), List("body2End"))


    testOperation[String, String](input, processForTest _, expected, ordered = false)
  }
}
