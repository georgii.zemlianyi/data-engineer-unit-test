import JobDStream.{ParquetWriter, createContext, processStream}
import io.github.embeddedkafka.{EmbeddedKafka, EmbeddedKafkaConfig}
import org.apache.kafka.clients.consumer.ConsumerRecord
import org.apache.kafka.common.serialization.{Deserializer, Serializer, StringDeserializer, StringSerializer}
import org.apache.spark.sql.Row
import org.apache.spark.sql.types.{StringType, StructField, StructType}
import org.apache.spark.streaming.dstream.InputDStream
import org.apache.spark.streaming.kafka010.{ConsumerStrategies, KafkaUtils, LocationStrategies}
import org.scalatest.concurrent.Eventually
import org.scalatest.flatspec.AnyFlatSpec
import testutil.SparkStreamingSpec

import scala.collection.mutable.ListBuffer

// ingestion time, event time, and processing time
class KafkaDStreamManualSpec extends AnyFlatSpec with SparkStreamingSpec with EmbeddedKafka  with Eventually {

  "kafka DStream with manual clock" should "use ingestion time" in {
    // Stream setup
    EmbeddedKafka.start()
    createCustomTopic(topic = topic, partitions = 1)
    val results = ListBuffer.empty[Row]
    // Create input stream
    val inputStream: InputDStream[ConsumerRecord[String, String]] = KafkaUtils.createDirectStream[String, String](
      ssc,
      LocationStrategies.PreferConsistent,
      ConsumerStrategies.Subscribe[String, String](topicsSet, kafkaParams)
    )
    (1 to 5).foreach(i => publishToKafka(topic, "message_1_" + i))
    (1 to 5).foreach(i => publishToKafka(topic, "message_2_" + i))
    // Act
    processStream(inputStream, spark, schema)(df => results ++= df.collect())

    ssc.start()

    advanceClockOneBatch()
    deleteTopics(List(topic))
    EmbeddedKafka.stop()
    // assert
    val expected = Array(Row("message_1_1"), Row("message_1_2"), Row("message_1_3"), Row("message_1_4"), Row("message_1_5"))
    eventually(
      assert(results.sameElements(expected))
    )
  }

  val topic = "topic1"
  val kafkaParams = Map[String, String](
    "bootstrap.servers" -> "localhost:9092",
    "key.deserializer" -> "org.apache.kafka.common.serialization.StringDeserializer",
    "value.deserializer" -> "org.apache.kafka.common.serialization.StringDeserializer",
    "group.id" -> "groupId1",
    "auto.offset.reset" -> "earliest"
  )
  val topicsSet = Set(topic)
  implicit val embeddedKafkaConfig: EmbeddedKafkaConfig = EmbeddedKafkaConfig(kafkaPort = 9092)
  implicit val serializer: Serializer[String] = new StringSerializer()
  implicit val deserializer: Deserializer[String] = new StringDeserializer()
  val schema = new StructType()
    .add(StructField("Name", StringType, false))
}
